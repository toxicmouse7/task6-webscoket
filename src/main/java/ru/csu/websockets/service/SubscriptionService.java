package ru.csu.websockets.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

public interface SubscriptionService {
    /**
     * Обработка подключения пользователя.
     *
     * @param session сессия ws
     */
    void subscribe(WebSocketSession session);

    void handleMessage(WebSocketSession session, TextMessage message);
}
